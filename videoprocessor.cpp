#include "videoprocessor.h"


VideoProcessor::VideoProcessor(QObject *parent) : QObject(parent)
{

}

void VideoProcessor::startVideo() {
    cv::VideoCapture camera("../opencv-qt-starter/videos/Sample.mp4");
    cv::Mat inFrame, outFrame;
    stopped = false;
    camera.set(cv::CAP_PROP_FPS, 30);


    while(camera.isOpened() && !stopped) {
        camera >> inFrame;
        if(inFrame.empty())
            continue;


        cv::resize(inFrame, inFrame,cv::Size(640,480), 0, 0, cv::INTER_CUBIC);
        outFrame = inFrame.clone();
        // Adjust outFrame here instead of clone

        QImage faceImage = QImage(outFrame.data,
                                  outFrame.cols,
                                  outFrame.rows,
                                  outFrame.step,
                                  QImage::Format_RGB888)
                                    .rgbSwapped();
        emit outDisplay(QPixmap::fromImage(faceImage));
    }
}

void VideoProcessor::stopVideo() {
    stopped = true;
}

